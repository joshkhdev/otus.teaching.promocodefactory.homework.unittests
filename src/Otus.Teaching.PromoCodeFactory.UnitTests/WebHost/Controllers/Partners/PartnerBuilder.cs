﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Guid _id = Guid.Empty;

        private string _name = string.Empty;

        private int _numberIssuedPromoCodes = 0;

        private bool _isActive = true;

        private ICollection<PartnerPromoCodeLimit> _partnerLimits = new List<PartnerPromoCodeLimit>();

        public PartnerBuilder WithId(Guid id)
        {
            _id = id;

            return this;
        }

        public PartnerBuilder WithName(string name)
        {
            _name = name;

            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;

            return this;
        }

        public PartnerBuilder WithIsActive(bool isActive)
        {
            _isActive = isActive;

            return this;
        }

        public PartnerBuilder WithPartnerLimits(ICollection<PartnerPromoCodeLimit> partnerLimits)
        {
            _partnerLimits = partnerLimits;
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = _id,
                Name = _name,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                IsActive = _isActive,
                PartnerLimits = _partnerLimits,
            };
        }
    }
}
