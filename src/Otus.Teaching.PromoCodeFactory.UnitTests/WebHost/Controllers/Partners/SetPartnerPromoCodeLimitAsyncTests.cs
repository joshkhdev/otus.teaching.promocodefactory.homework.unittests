﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _mockPartnersRepository;

        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _mockPartnersRepository = fixture.Freeze<Mock<IRepository<Partner>>>();

            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// 1. Если партнер не найден, то выдать ошибку 404;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnNotFound()
        {
            // Arrange
            Partner nullPartner = null;

            var id = Guid.NewGuid();

            var request = new SetPartnerPromoCodeLimitRequestBuilder().Build();

            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(nullPartner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                id,
                request);

            // Assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то выдать ошибку 400;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(false)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder().Build();

            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3.1 Если партнеру выставляется лимит, то обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetLimit_NumberIssuedPromoCodesSetZero()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var limits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                    .WithPartnerId(partnerId)
                    .WithCreateDate(DateTime.Now.AddDays(-1))
                    .WithEndDate(DateTime.Now.AddDays(30))
                    .WithLimit(50)
                    .Build(),
            };

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithNumberIssuedPromoCodes(10)
                .WithPartnerLimits(limits)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(30)
                .WithEndDate(DateTime.Now.AddDays(31))
                .Build();


            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// 3.2 Если партнеру выставляется лимит, но лимит закончился, то количество промокодов не обнуляется;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetNewLimitAndPrevLimitEndDatePassed_NumberIssuedPromoCodesNotChange()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var limits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimitBuilder()
                    .WithPartnerId(partnerId)
                    .WithCreateDate(DateTime.Now.AddDays(-32))
                    .WithEndDate(DateTime.Now.AddDays(-1))
                    .WithLimit(50)
                    .Build(),
            };

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithNumberIssuedPromoCodes(10)
                .WithPartnerLimits(limits)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(30)
                .WithEndDate(DateTime.Now.AddDays(31))
                .Build();


            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(10);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetNewLimit_DisablePrevLimit()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var oldLimit = new PartnerPromoCodeLimitBuilder()
               .WithPartnerId(partnerId)
               .WithCreateDate(DateTime.Now.AddDays(-1))
               .WithEndDate(DateTime.Now.AddDays(30))
               .WithLimit(50)
               .Build();

            var limits = new List<PartnerPromoCodeLimit> { oldLimit };

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithNumberIssuedPromoCodes(10)
                .WithPartnerLimits(limits)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(30)
                .WithEndDate(DateTime.Now.AddDays(31))
                .Build();

            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            oldLimit.CancelDate.HasValue.Should().BeTrue();
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0;
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-10)]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetLimitLessOrEqualZero_ReturnBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(limit)
                .WithEndDate(DateTime.Now.AddDays(31))
                .Build();

            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetNewLimit_NewLimitSaved()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(10)
                .WithEndDate(DateTime.Now.AddDays(31))
                .Build();

            _mockPartnersRepository
                .Setup(partners => partners.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                partnerId,
                request);

            // Assert
            _mockPartnersRepository.Verify(partners => partners.UpdateAsync(partner), Times.Once());
        }

    }
}